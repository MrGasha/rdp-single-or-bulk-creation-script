import csv
from Input.template import *
FILENAME = '{networkname} - {host} - {username}.rdp'

def bulk_create():
    print('bulk creating')
    with open('Input/parameters.csv') as file:
        parameters = csv.reader(file)
        next(parameters)
        for username, host, networkname in parameters:
            new_rdp = rdp_template.format(host=host,username=username)
            filename = FILENAME.format(networkname=networkname, host=host, username=username)
            create_file(filename, new_rdp)
        print('finished files bulk creation')

def singular_creates():
    print('singular creating')
    another = 'yes'
    while another == 'yes' or another == 'y':
        host = input('host or ip: ')
        networkname = input('network name: ')
        username = input('username: ')

        new_rdp = rdp_template.format(host=host,username=username)
        filename = FILENAME.format(networkname=networkname, host=host, username=username)
        create_file(filename, new_rdp)
        another = input('do you want to continue? type yes/no or Y/N :').casefold()
    print('finished single file creations')


def create_file(filename:str, new_rdp:str):
    with open('Output/{}'.format(filename),'w' ) as output_rdp:
        output_rdp.write(new_rdp)


options = int(input('For singular creates type 1 or for bulk creation using a csv file type 2: '))

if options==1:
    singular_creates()
else:
    bulk_create()
